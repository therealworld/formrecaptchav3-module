<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\FormReCaptchaV3Module\Application\Component;

use Exception;
use JsonException;
use ReflectionClass;
use TheRealWorld\FormReCaptchaV3Module\Core\FormReCaptchaV3Helper;

/**
 * User object manager.
 *
 * @mixin \OxidEsales\Eshop\Application\Component\UserComponent
 */
class UserComponent extends UserComponent_parent
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws Exception
     * @throws JsonException
     */
    public function createUser()
    {
        if (FormReCaptchaV3Helper::checkReCaptchaV3((new ReflectionClass($this))->getShortName())) {
            return false;
        }

        return parent::createUser();
    }
}
