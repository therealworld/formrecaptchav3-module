<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\FormReCaptchaV3Module\Core;

use TheRealWorld\ToolsPlugin\Traits\ViewConfigTrait;

class ViewConfig extends ViewConfig_parent
{
    use ViewConfigTrait;

    /** Template variable getter. Check if the captcha should be integrated into current view  */
    public function integrateReCaptchaV3(): bool
    {
        return FormReCaptchaV3Helper::isReCaptchaV3IntegratedInFrontendController(
            $this->getCurrentFrontendControllerShort()
        );
    }
}
