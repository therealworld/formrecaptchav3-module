<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\FormReCaptchaV3Module\Core;

use Exception;
use JsonException;
use OxidEsales\Eshop\Core\Exception\InputException;
use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\ToolsPlugin\Core\ToolsApi;
use TheRealWorld\ToolsPlugin\Core\ToolsMonologLogger;

/**
 * central Configuration for FormReCaptchaV3.
 */
class FormReCaptchaV3Helper
{
    /** the formelements*/
    protected static array $_aFormElements = [
        'invadr',
        'pa',
        'editval',
        'lgn_usr',
    ];

    /** the FrontendController with ReCaptchaV3 Checks */
    protected static array $_aControllerWithReCaptchaV3 = [
        'UserComponent'            => 'User',
        'UserController'           => 'User',
        'InviteController'         => 'User',
        'RegisterController'       => 'User',
        'ContactController'        => 'Contact',
        'SuggestController'        => 'Details',
        'ArticleDetailsController' => 'Details',
        'PriceAlarmController'     => 'Details',
        'NewsletterController'     => 'Newsletter',
        'ForgotPasswordController' => 'Password',
    ];

    /** get the FrontendController with ReCaptchaV3 Checks */
    public static function isReCaptchaV3IntegratedInFrontendController($sFrontendController = ''): bool
    {
        $bResult = false;
        if ($sOption = self::$_aControllerWithReCaptchaV3[$sFrontendController]) {
            $bResult = (bool) Registry::getConfig()->getConfigParam('bTRWFormReCaptchaV3Integration' . $sOption);
        }

        return $bResult;
    }

    /**
     * check Google ReCaptchaV3.
     *
     * @param string $sFrontendController - FrontendController
     *
     * @throws JsonException
     * @throws Exception
     * @throws Exception
     */
    public static function checkReCaptchaV3(string $sFrontendController): bool
    {
        $bResult = false;
        $oConfig = Registry::getConfig();
        $oRequest = Registry::getRequest();

        if (
            $oConfig->getConfigParam('bTRWFormReCaptchaV3UseReCaptchaV3')
            && self::isReCaptchaV3IntegratedInFrontendController($sFrontendController)
        ) {
            $bThrowExceptionOnError = $oConfig->getConfigParam('bTRWFormReCaptchaV3ThrowExceptionOnError');
            $bThrowMessageOnError = $oConfig->getConfigParam('bTRWFormReCaptchaV3ThrowMessageOnError');

            // check if request not valid ReCaptchaV3
            $bNotValidReCaptchaV3 = false;
            $sApiUrl = 'https://www.google.com/recaptcha/api/siteverify';
            $requestParams = [
                'secret'   => $oConfig->getConfigParam('sTRWFormReCaptchaV3SecretKey'),
                'response' => $oRequest->getRequestParameter('recaptcha_response'),
            ];

            if (ini_get('allow_url_fopen')) {
                $sRecaptchaResult = ToolsApi::executeExternApiFileGetContents(
                    $sApiUrl,
                    $requestParams
                );
            } else {
                $aRecaptchaResult = ToolsApi::executeExternApiGuzzle(
                    $sApiUrl,
                    'GET',
                    $requestParams
                );
                $sRecaptchaResult = $aRecaptchaResult['result'];
            }
            $aRecaptchaResult = json_decode($sRecaptchaResult, true, 512, JSON_THROW_ON_ERROR);

            // Take action based on the score returned
            // According to the documentation,
            // 1.0 is very likely a good interaction,
            // 0.0 is very likely a bot'.
            // For simplicity, I'm accepting all submissions
            // from any user with a score of 0.5 or above.
            if (
                (int) $aRecaptchaResult['success'] !== 1 ||
                (float) $aRecaptchaResult['score'] <= (float) $oConfig->getConfigParam('sTRWFormReCaptchaV3Score')
            ) {
                $bNotValidReCaptchaV3 = true;
            }

            // collect Controller & Google Result
            $aRequestData = [
                'controller'   => $sFrontendController,
                'googleresult' => $aRecaptchaResult,
            ];

            // collect IP
            if ($oConfig->getConfigParam('bTRWFormReCaptchaV3CollectIP')) {
                $aRequestData['clientIP'] = $_SERVER['REMOTE_ADDR'];
            }

            // collect full request
            foreach (self::$_aFormElements as $sFormElements) {
                if ($mRequestParameter = $oRequest->getRequestParameter($sFormElements)) {
                    if (is_array($mRequestParameter)) {
                        $aRequestData += $mRequestParameter;
                    } else {
                        $aRequestData += [$sFormElements => $mRequestParameter];
                    }
                }
            }

            // Error Handling
            if (($bThrowExceptionOnError || $bThrowMessageOnError) && $bNotValidReCaptchaV3) {
                $bResult = true;

                if ($bThrowMessageOnError) {
                    Registry::getUtilsView()->addErrorToDisplay('ERROR_MESSAGE_RECAPTCHAV3_MESSAGE');
                }
                if ($bThrowExceptionOnError) {
                    self::_writeToLog(
                        'ERROR_MESSAGE_RECAPTCHAV3_EXCEPTION',
                        $aRequestData
                    );
                }
            }

            // Log Handling
            if (self::_shouldWriteLogForFrontendController($sFrontendController)) {
                self::_writeToLog(
                    'INFO_MESSAGE_RECAPTCHAV3_EXCEPTION',
                    $aRequestData,
                    'info'
                );
            }
        }

        return $bResult;
    }

    /**
     * should write Log For FrontendController?
     */
    private static function _shouldWriteLogForFrontendController(string $sFrontendController = ''): bool
    {
        $bResult = false;
        if ($sOption = self::$_aControllerWithReCaptchaV3[$sFrontendController]) {
            $bResult = (bool) Registry::getConfig()->getConfigParam('bTRWFormReCaptchaV3WriteLogFor' . $sOption);
        }

        return $bResult;
    }

    /**
     * write a Exception Message to the log.
     *
     * @param string $sMessage  (oLang Variable)
     * @param string $sLogLevel (e.g. error, info, warning ... see Monolog\Logger
     *
     * @throws Exception
     */
    private static function _writeToLog(string $sMessage, array $aData = [], string $sLogLevel = 'error'): void
    {
        // help: build array to string without print_r
        $sData = str_replace('=', ':', http_build_query($aData, '', ','));

        $oEx = oxNew(InputException::class);
        $oEx->setMessage(sprintf(
            Registry::getLang()->translateString($sMessage),
            $sData
        ));

        $oLogger = ToolsMonologLogger::getLogger('trwformrecaptchav3');

        $oLogger->{$sLogLevel}($oEx->getMessage());
    }
}
