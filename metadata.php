<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// Metadata version.

use OxidEsales\Eshop\Application\Component\UserComponent as OxUserComponent;
use OxidEsales\Eshop\Application\Controller\ArticleDetailsController as OxArticleDetailsController;
use OxidEsales\Eshop\Application\Controller\ContactController as OxContactController;
use OxidEsales\Eshop\Application\Controller\ForgotPasswordController as OxForgotPasswordController;
use OxidEsales\Eshop\Application\Controller\InviteController as OxInviteController;
use OxidEsales\Eshop\Application\Controller\NewsletterController as OxNewsletterController;
use OxidEsales\Eshop\Application\Controller\PriceAlarmController as OxPriceAlarmController;
use OxidEsales\Eshop\Application\Controller\SuggestController as OxSuggestController;
use OxidEsales\Eshop\Core\ViewConfig as OxViewConfig;
use TheRealWorld\FormReCaptchaV3Module\Application\Component\UserComponent;
use TheRealWorld\FormReCaptchaV3Module\Application\Controller\ArticleDetailsController;
use TheRealWorld\FormReCaptchaV3Module\Application\Controller\ContactController;
use TheRealWorld\FormReCaptchaV3Module\Application\Controller\ForgotPasswordController;
use TheRealWorld\FormReCaptchaV3Module\Application\Controller\InviteController;
use TheRealWorld\FormReCaptchaV3Module\Application\Controller\NewsletterController;
use TheRealWorld\FormReCaptchaV3Module\Application\Controller\PriceAlarmController;
use TheRealWorld\FormReCaptchaV3Module\Application\Controller\SuggestController;
use TheRealWorld\FormReCaptchaV3Module\Core\ViewConfig;
use TheRealWorld\ToolsPlugin\Core\ToolsModuleVersion;

$sMetadataVersion = '2.1';

/**
 * Module information.
 */
$aModule = [
    'id'    => 'trwformrecaptchav3',
    'title' => [
        'de' => 'the-real-world - Formular Google ReCaptcha V3 Schutz',
        'en' => 'the-real-world - Form Google ReCaptcha V3 Protection',
    ],
    'description' => [
        'de' => 'A Google-Recaptcha-V3-Module für öffentliche Formulare z.b. Kontaktformular in OXID6.',
        'en' => 'A Google-Recaptcha-V3-Module for public forms like contact-form in OXID6.',
    ],
    'thumbnail' => 'picture.png',
    'version'   => ToolsModuleVersion::getModuleVersion('trwformrecaptchav3'),
    'author'    => 'Mario Lorenz',
    'url'       => 'https://www.the-real-world.de',
    'email'     => 'mario_lorenz@the-real-world.de',
    'extend'    => [
        // Core
        OxViewConfig::class => ViewConfig::class,
        // Component
        OxUserComponent::class => UserComponent::class,
        // Frontend-Controller
        OxArticleDetailsController::class => ArticleDetailsController::class,
        OxContactController::class        => ContactController::class,
        OxInviteController::class         => InviteController::class,
        OxForgotPasswordController::class => ForgotPasswordController::class,
        OxNewsletterController::class     => NewsletterController::class,
        OxPriceAlarmController::class     => PriceAlarmController::class,
        OxSuggestController::class        => SuggestController::class,
    ],
    'blocks' => [
        [
            'template' => 'form/contact.tpl',
            'block'    => 'captcha_form',
            'file'     => '/Application/views/blocks/form_recaptchav3.tpl',
        ],
        [
            'template' => 'form/newsletter.tpl',
            'block'    => 'captcha_form',
            'file'     => '/Application/views/blocks/form_recaptchav3.tpl',
        ],
        [
            'template' => 'form/privatesales/invite.tpl',
            'block'    => 'captcha_form',
            'file'     => '/Application/views/blocks/form_recaptchav3.tpl',
        ],
        [
            'template' => 'form/pricealarm.tpl',
            'block'    => 'captcha_form',
            'file'     => '/Application/views/blocks/form_recaptchav3.tpl',
        ],
        [
            'template' => 'form/suggest.tpl',
            'block'    => 'captcha_form',
            'file'     => '/Application/views/blocks/form_recaptchav3.tpl',
        ],
        [
            'template' => 'form/fieldset/user_billing.tpl',
            'block'    => 'form_user_billing_country',
            'file'     => '/Application/views/blocks/form_recaptchav3.tpl',
        ],
        [
            'template' => 'form/forgotpwd_email.tpl',
            'block'    => 'captcha_form',
            'file'     => '/Application/views/blocks/form_recaptchav3.tpl',
        ],
    ],
    'settings' => [
        [
            'group' => 'trwformrecaptchav3control',
            'name'  => 'bTRWFormReCaptchaV3UseReCaptchaV3',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwformrecaptchav3control',
            'name'  => 'bTRWFormReCaptchaV3ThrowMessageOnError',
            'type'  => 'bool',
            'value' => true,
        ],
        [
            'group' => 'trwformrecaptchav3control',
            'name'  => 'bTRWFormReCaptchaV3ThrowExceptionOnError',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwformrecaptchav3control',
            'name'  => 'bTRWFormReCaptchaV3ThrowMessageOnError',
            'type'  => 'bool',
            'value' => true,
        ],
        [
            'group' => 'trwformrecaptchav3control',
            'name'  => 'bTRWFormReCaptchaV3ThrowExceptionOnUse',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwformrecaptchav3control',
            'name'  => 'bTRWFormReCaptchaV3CollectIP',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwformrecaptchav3control',
            'name'  => 'bTRWFormReCaptchaV3WriteLogForUser',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwformrecaptchav3control',
            'name'  => 'bTRWFormReCaptchaV3WriteLogForNewsletter',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwformrecaptchav3control',
            'name'  => 'bTRWFormReCaptchaV3WriteLogForContact',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwformrecaptchav3control',
            'name'  => 'bTRWFormReCaptchaV3WriteLogForDetails',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwformrecaptchav3control',
            'name'  => 'bTRWFormReCaptchaV3WriteLogForPassword',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwformrecaptchav3googleapi',
            'name'  => 'sTRWFormReCaptchaV3WebsiteKey',
            'type'  => 'str',
            'value' => '',
        ],
        [
            'group' => 'trwformrecaptchav3googleapi',
            'name'  => 'sTRWFormReCaptchaV3SecretKey',
            'type'  => 'str',
            'value' => '',
        ],
        [
            'group'       => 'trwformrecaptchav3googleapi',
            'name'        => 'sTRWFormReCaptchaV3Score',
            'type'        => 'select',
            'value'       => '0.5',
            'constraints' => '0.0|0.1|0.2|0.3|0.4|0.5|0.6|0.7|0.8|0.9|1.0',
        ],
        [
            'group' => 'trwformrecaptchav3integrations',
            'name'  => 'bTRWFormReCaptchaV3IntegrationUser',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwformrecaptchav3integrations',
            'name'  => 'bTRWFormReCaptchaV3IntegrationNewsletter',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwformrecaptchav3integrations',
            'name'  => 'bTRWFormReCaptchaV3IntegrationContact',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwformrecaptchav3integrations',
            'name'  => 'bTRWFormReCaptchaV3IntegrationDetails',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwformrecaptchav3integrations',
            'name'  => 'bTRWFormReCaptchaV3IntegrationPassword',
            'type'  => 'bool',
            'value' => false,
        ],
    ],
];
