<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\FormReCaptchaV3Module\Application\Controller;

use JsonException;
use ReflectionClass;
use TheRealWorld\FormReCaptchaV3Module\Core\FormReCaptchaV3Helper;

/**
 * Newsletter opt-in/out.
 *
 * @mixin \OxidEsales\Eshop\Application\Controller\NewsletterController
 */
class NewsletterController extends NewsletterController_parent
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws JsonException
     */
    public function send()
    {
        if (FormReCaptchaV3Helper::checkReCaptchaV3((new ReflectionClass($this))->getShortName())) {
            return false;
        }

        return parent::send();
    }
}
